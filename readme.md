ansible-bootstrap
=================

Gets Ansible up and running on a fresh install of Ubuntu Server 18.04. Meant to be run on a local virtual machine, a sort of "poor man's Docker container."

These scripts are for running on my **personal virtual machines**. If you use this repository without modifying the `resources/user-ssh-key.pub` file, you will be giving my laptop access to your new server.

On your freshly-installed server, run this:

```
git clone https://pcrockett@bitbucket.org/pcrockett/ansible-bootstrap.git
cd ansible-bootstrap
./bootstrap
```

Then if you want to start deploying [Streisand](https://github.com/StreisandEffect/streisand) servers on [Linode](https://www.linode.com/):

```
./streisand-linode
```
