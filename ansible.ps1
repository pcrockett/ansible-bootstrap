#!/usr/bin/pwsh

$ErrorActionPreference = "Stop"
Set-StrictMode -Version 5.0

function exec($command) {
    & $command $args
    $result = $LASTEXITCODE
    if ($result -ne 0) {
        throw "$command $args exited with code $result"
    }
}

# From Ansible's install docs
# https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html
exec sudo apt-add-repository --yes ppa:ansible/ansible
exec sudo apt-get update
exec sudo apt-get --yes install ansible
