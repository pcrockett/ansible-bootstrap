#!/usr/bin/pwsh

$ErrorActionPreference = "Stop"
Set-StrictMode -Version 5.0

# Create SSH key that will be used to allow THIS server to configure other servers
# There's a bug in PowerShell preventing us from passing empty strings as parameters. :(
# It's an issue on Github
# https://github.com/PowerShell/PowerShell/issues/6280
# Here is our workaround:
"ssh-keygen -f ~/.ssh/id_rsa -N `"`"" | /bin/bash
$result = $LASTEXITCODE
if ($result -ne 0) {
    throw "ssh-keygen exited with code $result"
}

$thisDir = Split-Path $MyInvocation.MyCommand.Path -Parent
$pubKeyFile = Join-Path $thisDir "resources/user-ssh-key.pub"

# Authorize Phil's laptop to SSH into THIS server.
Copy-Item $pubKeyFile ~/.ssh/authorized_keys

Write-Host -ForegroundColor Green "SSH access enabled. Use 'ifconfig' to determine what IP address to SSH into."
