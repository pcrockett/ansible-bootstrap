#!/usr/bin/pwsh

$ErrorActionPreference = "Stop"
Set-StrictMode -Version 5.0

function exec($command) {
    & $command $args
    $result = $LASTEXITCODE
    if ($result -ne 0) {
        throw "$command $args exited with code $result"
    }
}

$thisDir = Split-Path $MyInvocation.MyCommand.Path -Parent
$gpgKeyFile = Join-Path $thisDir "resources/gpg-phil.asc"

exec gpg --import $gpgKeyFile
exec gpg --recv-keys
