#!/usr/bin/pwsh
#
# Designed to be run after the bootstrap script. Gets the server to start using
# Streisand (https://github.com/StreisandEffect/streisand), using Linode as the
# cloud provider for new Streisand servers.

$ErrorActionPreference = "Stop"
Set-StrictMode -Version 5.0

function exec($command) {
    & $command $args
    $result = $LASTEXITCODE
    if ($result -ne 0) {
        throw "$command $args exited with code $result"
    }
}

# From Streisand README
exec sudo apt-get install --yes python-paramiko python-pip python-pycurl python-dev build-essential
exec sudo pip install linode-python

$streisandHome = "~/streisand"
if (!(Test-Path $streisandHome)) {
    exec git clone https://github.com/StreisandEffect/streisand.git $streisandHome
}

$thisDir = Split-Path $MyInvocation.MyCommand.Path -Parent
$streisandConfigFile = Join-Path $thisDir "resources/streisand/site.yml"

Push-Location
Set-Location $streisandHome

deploy/streisand-new-cloud-server.sh `
    --provider linode `
    --site-config $streisandConfigFile

$philKeyId = "56A12A9F"
$outputStreisandInstructions = "~/streisand.html.pgp"
gpg --encrypt --recipient $philKeyId --output $outputStreisandInstructions generated-docs/streisand.html

Pop-Location

Write-Host -ForegroundColor Green "Streisand instructions can be found in $outputStreisandInstructions. Run a command like this on your local computer to download connection instructions:"
Write-Host -ForegroundColor Green "scp $env:USER@[ansible-server-ip]:$outputStreisandInstructions ."
Write-Host -ForegroundColor Green "Don't forget to run ./enable-ssh-access.ps1 root@[streisand-server-ip]"
