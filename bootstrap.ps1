#!/usr/bin/pwsh

$ErrorActionPreference = "Stop"
Set-StrictMode -Version 5.0

$thisDir = Split-Path $MyInvocation.MyCommand.Path -Parent
& $thisDir/ansible.ps1
& $thisDir/ssh.ps1
& $thisDir/gpg.ps1

Write-Host -ForegroundColor Green "Bootstrap finished. If you want to create a Streisand server, run streisand-linode.ps1."
