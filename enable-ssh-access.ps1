#!/usr/bin/pwsh
[CmdletBinding()]
param(

    [Parameter(Mandatory=$True)]
    [string]$UserHost
)

$ErrorActionPreference = "Stop"
Set-StrictMode -Version 5.0

$thisDir = Split-Path $MyInvocation.MyCommand.Path -Parent
$sshPubKey = Join-Path $thisDir "resources/user-ssh-key.pub"

@("`n") + (Get-Content $sshPubKey) | ssh $UserHost "cat >> ~/.ssh/authorized_keys"
$result = $LASTEXITCODE
if ($result -ne 0) {
    throw "ssh exited with code $result."
}
